package DomSaxXstream;

import org.xml.sax.Attributes;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

public class DomSaxXstream {

	public static void main(String[] args) {

		XMLReader procesadorXML;
		try {

			GestionContenido gestor = new GestionContenido();
			MyErrorHandler error = new MyErrorHandler();

			System.out.println("***********************************");

			procesadorXML = XMLReaderFactory.createXMLReader();

			procesadorXML.setFeature("http://xml.org/sax/features/validation", true);
			procesadorXML.setErrorHandler(error);

			procesadorXML.setContentHandler(gestor);

			InputSource fileXML = new InputSource("kimetsu.xml");

			procesadorXML.parse(fileXML);
			System.out.println("***********************************");

		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}

class GestionContenido extends DefaultHandler {

	public GestionContenido() {
		super();
	}

	public void startDocument() throws SAXException {
		System.out.println("<Comienzo del Documento XML:/>");
		System.out.println("-----------------------------------------------------");
	}

	public void endDocument() {
		System.out.println("<Final del Documento XML/>");
	}

	public void startElement(String uri, String nombre, String nombreC, Attributes atts) {
		System.out.println("\t<" + nombre + ">");

	}

	// */
	public void endElement(String uri, String nombre, String nombreC) {
		System.out.println("\t</" + nombre + ">");
		System.out.println("-----------------------------------------------------");
	}

	public void characters(char[] ch, int inicio, int longitud) throws SAXException {
		String car = new String(ch, inicio, longitud);
		car = car.replaceAll("[\t\n]", "");
		if (!car.isEmpty())

			System.out.println("\tCaracteres: " + car);

	}

}

class MyErrorHandler implements ErrorHandler {

	private String getParseExceptionInfo(SAXParseException spe) {
		String systemId = spe.getSystemId();

		if (systemId == null) {
			systemId = "null";
		}

		String info = "URI=" + systemId + " Line=" + spe.getLineNumber() + ": " + spe.getMessage();

		return info;
	}

	public void warning(SAXParseException spe) throws SAXException {
		System.out.println("Warning: " + getParseExceptionInfo(spe));
	}

	public void error(SAXParseException spe) throws SAXException {
		String message = "Error: " + getParseExceptionInfo(spe);
		throw new SAXException(message);
	}

	public void fatalError(SAXParseException spe) throws SAXException {
		String message = "Fatal Error: " + getParseExceptionInfo(spe);
		throw new SAXException(message);
	}

}
